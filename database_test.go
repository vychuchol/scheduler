package scheduler

import (
	"testing"
)

type testIsOverlapped struct {
	f1     int64
	t1     int64
	f2     int64
	t2     int64
	result bool
}

func TestIsOverlapped(t *testing.T) {
	tests := []testIsOverlapped{
		{0, 0, 0, 0, true},
		{0, 10, 0, 20, true},
		{0, 20, 0, 10, true},

		{5, 10, 5, 8, true},
		{5, 10, 5, 10, true},
		{5, 10, 5, 15, true},

		{5, 10, 7, 8, true},
		{5, 10, 7, 10, true},
		{5, 10, 7, 15, true},

		{5, 10, 0, 3, false},
		{5, 10, 0, 5, false},
		{5, 10, 0, 8, true},
		{5, 10, 0, 10, true},
		{5, 10, 0, 15, true},

		{5, 10, 12, 15, false},
	}
	for _, test := range tests {
		if isOverlapped(test.f1, test.t1-test.f1, test.f2, test.t2-test.f2) != test.result {
			t.Errorf("invalid result for parameters (%d, %d, %d, %d)", test.f1, test.t1-test.f1, test.f2, test.t2-test.f2)
		}
	}
}
