package scheduler

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
)

var (
	database   *sql.DB
	statements = make(map[string]*sql.Stmt)
	queries    = map[string]string{

		"check_credentials": `
			SELECT id 
			FROM users 
			WHERE username = $1 AND password = $2 AND deleted IS NULL`,

		"is_editor": `
			SELECT count(*) 
			FROM roles 
			WHERE user_id = $1 AND employee_id = $2 AND name = 'editor' AND deleted IS NULL`,

		"is_editor_of_task": `
			SELECT count(*) 
			FROM tasks t LEFT JOIN roles r ON t.employee_id = r.employee_id
			WHERE r.user_id = $1 AND t.id = $2 AND r.name = 'editor' AND t.deleted IS NULL AND r.deleted IS NULL`,

		"list_users": `
			SELECT id, username
			FROM users 
			WHERE deleted IS NULL 
			ORDER BY username ASC`,

		"list_task_types": `
			SELECT id, name, color
			FROM task_types 
			WHERE deleted IS NULL 
			ORDER BY name ASC`,

		"list_employees": `
			SELECT e.id, e.name, r.name as role
			FROM roles r LEFT JOIN employees e ON r.employee_id = e.id
			WHERE r.deleted IS NULL AND e.deleted IS NULL AND r.user_id = $1
			ORDER BY r.seq ASC, e.name ASC`,

		"get_tasks": `
			SELECT
				t.id, 
				t.hour, 
				t.minute, 
				t.duration, 
				t.user_id, 
				t.employee_id, 
				t.task_type_id, 
				t.name, 
				t.description, 
				t.created 
			FROM tasks t INNER JOIN roles r ON t.employee_id = r.employee_id
			WHERE 
				t.year = $1 
				AND 
				t.month = $2 
				AND 
				t.day = $3 
				AND 
				t.deleted IS NULL 
				AND 
				r.user_id = $4 
				AND 
				(r.name = 'viewer' OR r.name = 'editor') 
				AND 
				r.deleted IS NULL`,

		"create_task": `
			INSERT INTO tasks (year, month, day, hour, minute, duration, user_id, employee_id, task_type_id, name, description) 
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
			RETURNING id`,

		"modify_task": `
			UPDATE tasks SET 
				year = $1,
				month = $2,
				day = $3,
				hour = $4,
				minute = $5,
				duration = $6,
				user_id = $7,
				employee_id = $8,
				task_type_id = $9,
				name = $10,
				description = $11 
			WHERE id = $12 AND deleted IS NULL`,

		"move_task": `
			UPDATE tasks SET 
				year = $1,
				month = $2,
				day = $3
			WHERE id = $4 AND deleted IS NULL`,

		"reassign_task": `
			UPDATE tasks SET 
				employee_id = $1
			WHERE id = $2 AND deleted IS NULL`,

		"delete_task": `
			UPDATE tasks
			SET deleted = now()
			WHERE id = $1 AND deleted IS NULL`,

		"get_task_overlapped_details": `
			SELECT t."employee_id", t."year", t."month", t."day", t."hour", t."minute", t."duration"
			FROM "tasks" t
			WHERE t."id" = $1 AND t."deleted" IS NULL`,

		"get_overlapped_tasks": `
			SELECT t."id", t."hour", t."minute", t."duration" 
			FROM "tasks" t
			WHERE t."year" = $1 AND t."month" = $2 AND t."day" = $3 AND t."employee_id" = $4 AND t."id" <> $5 AND t."deleted" IS NULL`,

		"log_action": `
			INSERT INTO "action_logs" ("action", "object", "user_id", "parameters")
			VALUES ($1, $2, $3, $4)`,
	}
)

type DB interface {
	Stmt(stmt *sql.Stmt) *sql.Stmt
}

type Tx struct {
	commited bool
	tx       *sql.Tx
}

func beginTx() (*Tx, error) {
	tx, err := database.Begin()
	if err != nil {
		return nil, err
	}
	return &Tx{
		commited: false,
		tx:       tx,
	}, nil
}

func (tx *Tx) Commit() error {
	err := tx.tx.Commit()
	if err == nil {
		tx.commited = true
	}
	return err
}

func (tx *Tx) Rollback() error {
	return tx.tx.Rollback()
}

func (tx *Tx) RollbackIfNotCommited() {
	if tx.commited {
		return
	}
	err := tx.tx.Rollback()
	if err != nil {
		log.Println(fmt.Sprintf("RollbackIfNotCommited: transaction rollback failed: %v", err))
	}
}

func (tx *Tx) Stmt(stmt *sql.Stmt) *sql.Stmt {
	return tx.tx.Stmt(stmt)
}

func getStmt(db DB, stmt *sql.Stmt) *sql.Stmt {
	if db == nil {
		return stmt
	}
	return db.Stmt(stmt)
}

func initAllStatements(database *sql.DB) {
	for name, query := range queries {
		stmt, err := database.Prepare(query)
		if err != nil {
			log.Fatalf("initAllStatements: %s: %v", name, err)
		}
		statements[name] = stmt
	}
}

// checkCredentials returns user id if credentials are correct.
// Otherwise returns 0.
func checkCredentials(db DB, username, password string) int64 {
	id := int64(0)
	stmt := getStmt(db, statements["check_credentials"])
	if err := stmt.QueryRow(username, password).Scan(&id); err != nil {
		return 0
	}
	return id
}

// isEditor returns true if user is editor of employee.
// Otherwise returns false.
func isEditor(db DB, userID, employeeID int64) bool {
	count := int64(0)
	stmt := getStmt(db, statements["is_editor"])
	if err := stmt.QueryRow(userID, employeeID).Scan(&count); err != nil {
		return false
	}
	return count > 0
}

// isEditorOfTask returns true if user is editor of task.
// Otherwise returns false.
func isEditorOfTask(db DB, userID, taskID int64) bool {
	count := int64(0)
	stmt := getStmt(db, statements["is_editor_of_task"])
	if err := stmt.QueryRow(userID, taskID).Scan(&count); err != nil {
		return false
	}
	return count > 0
}

func listUsers(db DB) []map[string]interface{} {
	stmt := getStmt(db, statements["list_users"])
	rows, err := stmt.Query()
	if err != nil {
		log.Println(fmt.Sprintf("%s: database error: %v", CallerName(0), err))
		return make([]map[string]interface{}, 0)
	}
	defer rows.Close()

	users := make([]map[string]interface{}, 0)
	for rows.Next() {
		var (
			id       int64
			username string
		)
		if err := rows.Scan(&id, &username); err != nil {
			log.Println(fmt.Sprintf("%s: database error during scan: %v", CallerName(0), err))
			return make([]map[string]interface{}, 0)
		}
		users = append(users, map[string]interface{}{
			"id":       id,
			"username": username,
		})
	}
	return users
}

func listTaskTypes(db DB) TaskTypes {
	stmt := getStmt(db, statements["list_task_types"])
	rows, err := stmt.Query()
	if err != nil {
		log.Println(fmt.Sprintf("%s: database error: %v", CallerName(0), err))
		return make(TaskTypes, 0)
	}
	defer rows.Close()

	var defaultTaskType *TaskType // id = 1
	taskTypes := make(TaskTypes, 0)
	for rows.Next() {
		var (
			id    int64
			name  string
			color int32
		)
		if err := rows.Scan(&id, &name, &color); err != nil {
			log.Println(fmt.Sprintf("%s: database error during scan: %v", CallerName(0), err))
			return make(TaskTypes, 0)
		}
		tt := &TaskType{
			ID:    id,
			Name:  name,
			Color: color,
		}
		if id == int64(1) {
			defaultTaskType = tt
			continue
		}
		taskTypes = append(taskTypes, tt)
	}
	r := make(TaskTypes, 0, len(taskTypes)+1)
	r = append(r, defaultTaskType)
	for _, tt := range taskTypes {
		r = append(r, tt)
	}
	return r
}

func isOverlapped(f1, d1, f2, d2 int64) bool {
	if f1 == f2 && d1 == d2 {
		return true
	}
	if f2 >= (f1 + d1) {
		return false
	}
	if (f2 + d2) <= f1 {
		return false
	}
	return true
}

func getTaskOverlappedDetails(db DB, taskID int64) (employeeID int64, year, month, day, hour, minute, duration int) {
	stmt := getStmt(db, statements["get_task_overlapped_details"])
	err := stmt.QueryRow(taskID).Scan(&employeeID, &year, &month, &day, &hour, &minute, &duration)
	if err != nil {
		log.Println(fmt.Sprintf("%s: database error: %v", CallerName(0), err))
		return 0, 0, 0, 0, 0, 0, 0
	}
	return employeeID, year, month, day, hour, minute, duration
}

func isSpaceForTask(db DB, employeeID int64, taskID int64, year, month, day, hour, minute, duration int) (bool, int64) {
	stmt := getStmt(db, statements["get_overlapped_tasks"])
	rows, err := stmt.Query(year, month, day, employeeID, taskID)
	if err != nil {
		log.Println(fmt.Sprintf("%s: database error: %v", CallerName(0), err))
		return false, 0
	}
	defer rows.Close()

	// This check must be same as scheduler.js Record.isOverlappedWith.
	from := int64(hour*60 + minute)

	for rows.Next() {
		var (
			id int64
			h  int64
			m  int64
			d  int64
		)
		if err := rows.Scan(&id, &h, &m, &d); err != nil {
			log.Println(fmt.Sprintf("%s: database error during scan: %v", CallerName(0), err))
			return false, 0
		}
		f := h*60 + m
		if isOverlapped(f, d, from, int64(duration)) {
			return false, id
		}
	}
	return true, 0
}

func pair(key string, value interface{}) string {
	v := fmt.Sprintf("%v", value)
	if strings.ContainsAny(v, " \t") {
		return fmt.Sprintf("%s=\"%v\"", key, strings.Replace(v, "\"", "\\\"", -1))
	}
	return fmt.Sprintf("%s=%v", key, v)
}

func logAction(action, object string, userID int64, parameters ...string) {
	p := strings.Join(parameters, " ")
	if _, err := statements["log_action"].Exec(action, object, userID, p); err != nil {
		log.Println(fmt.Sprintf("%s: database error: %v", CallerName(0), err))
		log.Println(fmt.Sprintf("CommitLog: action=%s object=%s user_id=%d parameters='%s'", action, object, userID, p))
	}
}
