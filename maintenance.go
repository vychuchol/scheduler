package scheduler

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	vychuchol "bitbucket.org/vychuchol/core"
)

type MaintenanceData struct {
	BackupCommandFormat string
}

func NewMaintenanceData(c vychuchol.Configuration) *MaintenanceData {
	dbname, host, port, user, _ := c.DatabaseParameters()
	return &MaintenanceData{
		BackupCommandFormat: fmt.Sprintf("pg_dump --dbname=%s --host=%s --port=%d --username=%s --data-only --file=%%s.sql", dbname, host, port, user),
	}
}

func (md *MaintenanceData) BackupCommand() string {
	return fmt.Sprintf(md.BackupCommandFormat, time.Now().Format("2006-01-02T150405"))
}

func (md *MaintenanceData) Process(path string, rw http.ResponseWriter) {
	if strings.HasPrefix(path, "/backup") {
		io.WriteString(rw, maintenanceData.BackupCommand())
	} else {
		io.WriteString(rw, "invalid action")
	}
}
