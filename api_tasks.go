package scheduler

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/vychuchol/datetime"
	"bitbucket.org/vychuchol/jsonrpc"
)

var apiTasks = &jsonrpc.Collection{
	Before: before,
	Methods: map[string]jsonrpc.Method{
		"ping": ping,
		"list_employees": listEmployees,
		"get_tasks":      getTasks,
		"create_task":    createTask,
		"modify_task":    modifyTask,
		"move_task":      moveTask,
		"reassign_task":  reassignTask,
		"delete_task":    deleteTask,
	},
}

func before(req *http.Request, method string, params map[string]interface{}, result map[string]interface{}) {
	var userID interface{}
	var username interface{}
	if s, err := sessions.Load(req); err == nil {
		if v, ok := s["userid"]; ok {
			userID = v
			username, _ = s["username"]
		}
	}
	params["session.userid"] = userID
	params["session.username"] = username
}

func ping(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	return jsonrpc.OK, ""
}

func listEmployees(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	rows, err := statements["list_employees"].Query(sessionUserID)
	if err != nil {
		message = fmt.Sprintf("%s: database error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	defer rows.Close()

	employees := make([]map[string]interface{}, 0)
	for rows.Next() {
		var (
			id   int64
			name string
			role string
		)
		if err := rows.Scan(&id, &name, &role); err != nil {
			message = fmt.Sprintf("%s: database error during scan: %v", CallerName(0), err)
			log.Println(message)
			return jsonrpc.ErrorInvalidRequest, message
		}
		employees = append(employees, map[string]interface{}{
			"id":   id,
			"name": name,
			"role": role,
		})
	}

	result["employees"] = employees
	return jsonrpc.OK, ""
}

func getTasks(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	dateRaw, ok := params["date"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing date", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}
	date, ok := datetime.ParseDate(dateRaw)
	if !ok {
		message = fmt.Sprintf("%s: invalid date: '%s'", CallerName(0), dateRaw)
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	rows, err := statements["get_tasks"].Query(date.Year, date.Month, date.Day, sessionUserID)
	if err != nil {
		message = fmt.Sprintf("%s: database error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	defer rows.Close()

	tasks := make([]map[string]interface{}, 0)
	for rows.Next() {
		var (
			id          int64
			hour        int64
			minute      int64
			duration    int64
			userID      int64
			employeeID  int64
			taskTypeID  int64
			name        string
			description string
			created     time.Time
		)
		if err := rows.Scan(&id, &hour, &minute, &duration, &userID, &employeeID, &taskTypeID, &name, &description, &created); err != nil {
			message = fmt.Sprintf("%s: database error during scan: %v", CallerName(0), err)
			log.Println(message)
			return jsonrpc.ErrorInvalidRequest, message
		}
		tasks = append(tasks, map[string]interface{}{
			"id":           id,
			"hour":         hour,
			"minute":       minute,
			"duration":     duration,
			"user_id":      userID,
			"employee_id":  employeeID,
			"task_type_id": taskTypeID,
			"name":         name,
			"description":  description,
			"created":      created,
		})
	}

	result["tasks"] = tasks
	return jsonrpc.OK, ""
}

func createTask(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	dateTimeRaw, ok := params["date_time"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing date_time", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}
	dateTime, ok := datetime.ParseDateTime(dateTimeRaw)
	if !ok {
		message = fmt.Sprintf("%s: invalid date_time: '%s'", CallerName(0), dateTimeRaw)
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	durationRaw, ok := params["duration"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing duration", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}
	duration, ok := datetime.ParseDuration(durationRaw)
	if !ok {
		message = fmt.Sprintf("%s: invalid duration: '%s'", CallerName(0), durationRaw)
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	employeeID, ok := jsonrpc.GetInt64(params, "employee_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid employee_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	taskTypeID, ok := jsonrpc.GetInt64(params, "task_type_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid task_type_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	name, ok := params["name"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing name", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	description, _ := params["description"].(string)

	// check permissions
	if !isEditor(nil, sessionUserID, employeeID) {
		message = fmt.Sprintf("%s: needed editor permission", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	// create transaction
	tx, err := beginTx()
	if err != nil {
		message = fmt.Sprintf("%s: can not begin transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	defer tx.RollbackIfNotCommited()

	// check overlap
	isSpace, overlappedTaskID := isSpaceForTask(tx, employeeID, 0, dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, duration.Minutes())
	if !isSpace {
		message = fmt.Sprintf("%s: overlapping tasks new and %d", CallerName(0), overlappedTaskID)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	row := getStmt(tx, statements["create_task"]).QueryRow(
		dateTime.Year,
		dateTime.Month,
		dateTime.Day,
		dateTime.Hour,
		dateTime.Minute,
		duration.Minutes(),
		sessionUserID,
		employeeID,
		taskTypeID,
		name,
		description,
	)
	var id int64
	if err := row.Scan(&id); err != nil {
		message = fmt.Sprintf("%s: database error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	// commit transaction
	if err := tx.Commit(); err != nil {
		message = fmt.Sprintf("%s: can not commit transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	result["task_id"] = id
	logAction("create", "task", sessionUserID, pair("id", id))
	return jsonrpc.OK, ""
}

func modifyTask(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	id, ok := jsonrpc.GetInt64(params, "task_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid task_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	dateTimeRaw, ok := params["date_time"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing date_time", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}
	dateTime, ok := datetime.ParseDateTime(dateTimeRaw)
	if !ok {
		message = fmt.Sprintf("%s: invalid date_time: '%s'", CallerName(0), dateTimeRaw)
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	durationRaw, ok := params["duration"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing duration", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}
	duration, ok := datetime.ParseDuration(durationRaw)
	if !ok {
		message = fmt.Sprintf("%s: invalid duration: '%s'", CallerName(0), durationRaw)
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	employeeID, ok := jsonrpc.GetInt64(params, "employee_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid employee_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	taskTypeID, ok := jsonrpc.GetInt64(params, "task_type_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid task_type_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	name, ok := params["name"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing name", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	description, _ := params["description"].(string)

	// check permissions
	if !isEditor(nil, sessionUserID, employeeID) {
		message = fmt.Sprintf("%s: needed editor permission", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	if !isEditorOfTask(nil, sessionUserID, id) {
		message = fmt.Sprintf("%s: needed editor permission", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	// create transaction
	tx, err := beginTx()
	if err != nil {
		message = fmt.Sprintf("%s: can not begin transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	defer tx.RollbackIfNotCommited()

	// check overlap
	isSpace, overlappedTaskID := isSpaceForTask(tx, employeeID, id, dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, duration.Minutes())
	if !isSpace {
		message = fmt.Sprintf("%s: overlapping tasks %d and %d", CallerName(0), id, overlappedTaskID)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	r, err := getStmt(tx, statements["modify_task"]).Exec(
		dateTime.Year,
		dateTime.Month,
		dateTime.Day,
		dateTime.Hour,
		dateTime.Minute,
		duration.Minutes(),
		sessionUserID,
		employeeID,
		taskTypeID,
		name,
		description,
		id,
	)
	if err != nil {
		message = fmt.Sprintf("%s: database error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	if c, err := r.RowsAffected(); err != nil {
		message = fmt.Sprintf("%s: database error: affected rows error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	} else if c != 1 {
		message = fmt.Sprintf("%s: database error: invalid affected rows count (1 != %d)", CallerName(0), c)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	// commit transaction
	if err := tx.Commit(); err != nil {
		message = fmt.Sprintf("%s: can not commit transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	result["task_id"] = id
	logAction("modify", "task", sessionUserID, pair("id", id))
	return jsonrpc.OK, ""
}

func moveTask(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	id, ok := jsonrpc.GetInt64(params, "task_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid task_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	dateRaw, ok := params["date"].(string)
	if !ok {
		message = fmt.Sprintf("%s: missing date", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}
	date, ok := datetime.ParseDate(dateRaw)
	if !ok {
		message = fmt.Sprintf("%s: invalid date: '%s'", CallerName(0), dateRaw)
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	// check permissions
	if !isEditorOfTask(nil, sessionUserID, id) {
		message = fmt.Sprintf("%s: needed editor permission", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	// create transaction
	tx, err := beginTx()
	if err != nil {
		message = fmt.Sprintf("%s: can not begin transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	defer tx.RollbackIfNotCommited()

	// check overlap
	employeeID, _, _, _, hour, minute, duration := getTaskOverlappedDetails(tx, id)
	if employeeID == 0 {
		message = fmt.Sprintf("%s: can not get overlapped task details", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	isSpace, overlappedTaskID := isSpaceForTask(tx, employeeID, id, date.Year, date.Month, date.Day, hour, minute, duration)
	if !isSpace {
		message = fmt.Sprintf("%s: overlapping tasks %d and %d", CallerName(0), id, overlappedTaskID)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	r, err := getStmt(tx, statements["move_task"]).Exec(
		date.Year,
		date.Month,
		date.Day,
		id,
	)
	if err != nil {
		message = fmt.Sprintf("%s: database error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	if c, err := r.RowsAffected(); err != nil {
		message = fmt.Sprintf("%s: database error: affected rows error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	} else if c != 1 {
		message = fmt.Sprintf("%s: database error: invalid affected rows count (1 != %d)", CallerName(0), c)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	// commit transaction
	if err := tx.Commit(); err != nil {
		message = fmt.Sprintf("%s: can not commit transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	result["task_id"] = id
	logAction("move", "task", sessionUserID, pair("id", id), pair("date", date))
	return jsonrpc.OK, ""
}

func reassignTask(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	id, ok := jsonrpc.GetInt64(params, "task_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid task_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	newEmployeeID, ok := jsonrpc.GetInt64(params, "employee_id")
	if !ok {
		message = fmt.Sprintf("%s: missing or invalid employee_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	// check permissions
	if !isEditorOfTask(nil, sessionUserID, id) {
		message = fmt.Sprintf("%s: needed editor permission", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	// create transaction
	tx, err := beginTx()
	if err != nil {
		message = fmt.Sprintf("%s: can not begin transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	defer tx.RollbackIfNotCommited()

	// check overlap
	currentEmployeeID, year, month, day, hour, minute, duration := getTaskOverlappedDetails(tx, id)
	if currentEmployeeID == 0 {
		message = fmt.Sprintf("%s: can not get overlapped task details", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}
	isSpace, overlappedTaskID := isSpaceForTask(tx, newEmployeeID, id, year, month, day, hour, minute, duration)
	if !isSpace {
		message = fmt.Sprintf("%s: overlapping tasks %d and %d", CallerName(0), id, overlappedTaskID)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	r, err := getStmt(tx, statements["reassign_task"]).Exec(
		newEmployeeID,
		id,
	)
	if err != nil {
		message = fmt.Sprintf("%s: database error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	if c, err := r.RowsAffected(); err != nil {
		message = fmt.Sprintf("%s: database error: affected rows error: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	} else if c != 1 {
		message = fmt.Sprintf("%s: database error: invalid affected rows count (1 != %d)", CallerName(0), c)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	// commit transaction
	if err := tx.Commit(); err != nil {
		message = fmt.Sprintf("%s: can not commit transaction: %v", CallerName(0), err)
		log.Println(message)
		return jsonrpc.ErrorInvalidRequest, message
	}

	result["task_id"] = id
	logAction("reassign", "task", sessionUserID, 
		pair("id", id), 
		pair("old_employee_id", currentEmployeeID), 
		pair("new_employee_id", newEmployeeID),
	)
	return jsonrpc.OK, ""
}

func deleteTask(method string, params map[string]interface{}, result map[string]interface{}) (e jsonrpc.Error, message string) {
	if params == nil {
		message = fmt.Sprintf("%s: params is nil", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	sessionUserID, ok := jsonrpc.GetInt64(params, "session.userid")
	if sessionUserID <= 0 || !ok {
		message = fmt.Sprintf("%s: user not logged in", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	taskID, ok := jsonrpc.GetInt64(params, "task_id")
	if !ok {
		message = fmt.Sprintf("%s: missing task_id", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	// check permissions
	if !isEditorOfTask(nil, sessionUserID, taskID) {
		message = fmt.Sprintf("%s: needed editor permission", CallerName(0))
		log.Println(message)
		return jsonrpc.ErrorInvalidParams, message
	}

	statements["delete_task"].Exec(taskID)
	logAction("delete", "task", sessionUserID, pair("id", taskID))
	return jsonrpc.OK, ""
}
