package main

import (
	"bitbucket.org/vychuchol/scheduler"
)

func main() {
	scheduler.Start()
}
