<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Plánovač</title>
		<!-- Font Awesome Free v5.0.13 -->
		<link href="/static/extern/fontawesome/css/fa-regular.min.css" rel="stylesheet" />
		<link href="/static/extern/fontawesome/css/fa-solid.min.css" rel="stylesheet" />
		<link href="/static/extern/fontawesome/css/fontawesome.min.css" rel="stylesheet" />
		<!-- Moment v2.20.1 -->
		<script src="/static/extern/moment-with-locales.js"></script>
		<!-- Holidays -->
		<script src="/static/holidays.js"></script>
		<!-- Pikaday v1.6.1 -->
		<link href="/static/extern/pikaday.css" rel="stylesheet" type="text/css" />
		<script src="/static/extern/pikaday.js"></script>
		<!-- Scheduler -->
		<link href="/static/scheduler.css" rel="stylesheet" type="text/css" />
		<script src="/static/scheduler.js"></script>
		<script src="/static/configuration.js"></script>
		<script>
			// set moment locale globally
			moment.locale("cs");

			Sch.userID = {{.userid}};
			{{range .tasktypes}}
			Sch.taskTypes.push({
				id: {{.ID}},
				name: "{{.Name}}",
				color: "{{.HexColor}}",
			});
			{{end}}

			function hideMessage() {
				let message = document.querySelector(".schedulerLogin .message");
				if (message) {
					message.style.display = "none";
				}
			}
			
			function logout() {
				window.location.href = "/logout";
			}
		</script>
	</head>
	<body{{if .username}} onload="Sch.init();"{{end}}>
		<header>
			<h1>Plánovač</h1>
			{{if .username}}
			<button id="actionButtonPreviousDay" title="Předchozí den">
				<i class="far fa-calendar-minus"></i>
			</button>
			<span class="separator"></span>
			<button id="actionButtonView" title="Detail úkolu">
				<i class="fas fa-info-circle"></i>
			</button>
			<button id="actionButtonEdit" title="Upravit úkol">
				<i class="far fa-edit"></i>
			</button>
			<button id="actionButtonMove" title="Přesunout úkol na jiný den">
				<i class="far fa-calendar"></i>
			</button>
			<button id="actionButtonReassign" title="Předat úkol jinému zaměstnanci">
				<i class="far fa-user"></i>
			</button>
			<button id="actionButtonDelete" title="Smazat úkol">
				<i class="far fa-trash-alt"></i>
			</button>
			<span class="separator"></span>
			<button id="actionButtonNextDay" title="Následující den">
				<i class="far fa-calendar-plus"></i>
			</button>
			<div class="profile">
				{{.username}}
			</div>
			<button onclick="logout();" title="Odhlásit">
				<i class="fas fa-sign-out-alt"></i>
			</button>
			{{end}}
		</header>
		{{if .username}}
		<main style="display: none;">
			<div id="schedulerEditTask">
				<div class="row horizontal">
					<label for="name">Název:</label>
					<input type="text" name="name" maxlength="255" />
				</div>
				<div class="row vertical">
					<label for="description">Popis:</label>
					<textarea name="description" maxlength="2048"></textarea>
				</div>
				<div class="row horizontal">
					<label for="taskType">Typ&nbsp;úkolu:</label>
					<select name="taskType">
						{{range .tasktypes}}
						<option value="{{.ID}}">{{.Name}}</option>
						{{end}}
					</select>
				</div>
				<div class="row horizontal">
					<label for="description">Datum:</label>
					<input type="text" name="date" disabled="disabled" maxlength="255" />
				</div>
				<div class="row timeAndDuration">
					<span class="label timeLabel">Čas:</span>
					<input class="hour" type="number" name="hour" min="0" max="23" maxlength="2" />
					<span class="timeDelimiter">:</span>
					<input class="minute" type="number" name="minute" min="0" max="59" step="15" maxlength="2" />
					<span class="label durationLabel">Doba:</span>
					<input class="duration" type="number" name="duration" min="0.25" max="24" step="0.25" maxlength="3" />
					<span class="durationUnits">hodin</span>
				</div>
			</div>
			<div id="schedulerMoveTask">
				<div class="row horizontal">
					<label for="name">Název:</label>
					<input type="text" name="name" maxlength="255" readonly="readonly" />
				</div>
				<div class="row horizontal">
					<label for="description">Datum:</label>
					<input type="text" name="date" disabled="disabled" maxlength="255" />
				</div>
			</div>
			<div id="schedulerReassignTask">
				<div class="row horizontal">
					<label for="name">Název:</label>
					<input type="text" name="name" maxlength="255" readonly="readonly" />
				</div>
				<div class="row horizontal">
					<label for="description">Zaměstnanec:</label>
					<select name="employee"></select>
				</div>
			</div>
			<div id="schedulerViewTask">
				<div class="row horizontal">
					<label for="name">Název:</label>
					<input type="text" name="name" maxlength="255" readonly="readonly" />
				</div>
				<div class="row vertical">
					<label for="description">Popis:</label>
					<textarea name="description" maxlength="2048" readonly="readonly"></textarea>
				</div>
			</div>
		</main>
		{{else}}
		<form class="schedulerLogin" action="/login" method="POST">
			<label for="username">Přihlašovací jméno:</label>
			{{if .freelogin}}
			<input id="username" name="username" type="text" maxlength="255" />
			{{else}}
			<select id="username" name="username">
				{{$login := .loginfail}}{{range .usernames}}
				<option value="{{.}}"{{if eq $login .}} selected="selected"{{end}}>{{.}}</option>
				{{end}}
			</select>
			{{end}}
			<label for="password">Heslo:</label>
			<input type="password" id="password" name="password" maxlength="255" oninput="hideMessage();" />
			<input type="submit" value="Přihlásit se" />
			{{if .loginfail}}
			<div class="message">Přihlášení selhalo.</div>
			{{end}}
		</form>
		{{end}}
		<footer>
			Plánovač v{{.version}}
		</footer>
	</body>
</html>