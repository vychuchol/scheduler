package scheduler

import (
	"fmt"
)

type TaskType struct {
	ID    int64
	Color int32
	Name  string
}

type TaskTypes []*TaskType

func (tt *TaskType) HexColor() string {
	r := (tt.Color >> 16) & 0xFF
	g := (tt.Color >> 8) & 0xFF
	b := tt.Color & 0xFF
	return fmt.Sprintf("#%02x%02x%02x", r, g, b)
}
