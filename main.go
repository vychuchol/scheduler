package scheduler

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	vychuchol "bitbucket.org/vychuchol/core"
	"bitbucket.org/vychuchol/jsonrpc"
)

const Version = "1.0.1"

var (
	maintenanceData *MaintenanceData
	templates       *template.Template = vychuchol.LoadTemplates("templates", nil)
	sessions        *vychuchol.Sessions
	settings        *Settings
	usernames       []string
	taskTypes       TaskTypes
)

func login(rw http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	username := req.FormValue("username")
	password := req.FormValue("password")
	if username != "" && password != "" {
		hashedPassword := vychuchol.HashPassword(password, settings.PasswordSalt)
		if userID := checkCredentials(nil, username, hashedPassword); userID != 0 {
			data := make(map[string]interface{})
			data["userid"] = userID
			data["username"] = username
			if err := sessions.Save(rw, data); err != nil {
				log.Println(err)
			}
			logAction("login", "user", userID,
				pair("address", req.RemoteAddr),
				pair("user_agent", req.Header.Get("User-Agent")),
			)
		} else {
			http.Redirect(rw, req, "/?loginfail="+username, http.StatusTemporaryRedirect)
		}
	}
	http.Redirect(rw, req, "/", http.StatusTemporaryRedirect)
}

func api(rw http.ResponseWriter, req *http.Request) {
	apiTasks.Handler(rw, req)
}

func maintenance(rw http.ResponseWriter, req *http.Request) {
	if s, err := sessions.Load(req); err == nil {
		sessionUserID, ok := jsonrpc.GetInt64(s, "userid")
		if sessionUserID == 1 && ok { // admin = 1
			path := req.URL.Path[12:] // len("/maintenance")
			maintenanceData.Process(path, rw)
			return
		}
	}
	http.Error(rw, "User admin is not logged in.", http.StatusForbidden)
}

func logout(rw http.ResponseWriter, req *http.Request) {
	s, err := sessions.Load(req)
	userID := int64(0)
	if err == nil {
		userID, _ = jsonrpc.GetInt64(s, "userid")
	}
	if err := sessions.Delete(rw); err != nil {
		log.Println(fmt.Sprintf("logout error: %v", err))
		http.Redirect(rw, req, "/", http.StatusTemporaryRedirect)
		return
	}
	logAction("logout", "user", userID,
		pair("address", req.RemoteAddr),
		pair("user_agent", req.Header.Get("User-Agent")),
	)
	http.Redirect(rw, req, "/", http.StatusTemporaryRedirect)
}

func handler(rw http.ResponseWriter, req *http.Request) {
	data := make(map[string]interface{})
	data["tasktypes"] = taskTypes
	data["version"] = Version

	if s, err := sessions.Load(req); err == nil {
		data["userid"] = s["userid"]
		data["username"] = s["username"]
	} else {
		data["freelogin"] = settings.FreeLogin
		data["loginfail"] = ""
		login := req.URL.Query().Get("loginfail")
		for _, username := range usernames {
			if username == login {
				data["loginfail"] = username
			}
		}
		data["usernames"] = usernames
	}

	templates.ExecuteTemplate(rw, "main.tpl", data)
}

func Start() {
	defer vychuchol.RedirectLog("scheduler.log").Close()

	log.Println("loading configuration")
	configuration, err := vychuchol.LoadConfiguration("scheduler.json")
	if err != nil {
		log.Println(fmt.Sprintf("can not load configuration: %v", err))
	}

	maintenanceData = NewMaintenanceData(configuration)

	log.Println("opening database")
	database, err = vychuchol.OpenDatabase(configuration.DatabaseConnectionString())
	if err != nil {
		log.Fatalln(fmt.Sprintf("can not open database: %v", err))
	}

	log.Println("checking database")
	if err := vychuchol.CheckDatabaseSchemaVersion(database, "./db", 5); err != nil {
		log.Fatalln(err)
	}

	log.Println("preparing database statements")
	initAllStatements(database)

	log.Println("initialising sessions")
	sessions = vychuchol.NewSessions(configuration.SessionsConfiguration())

	log.Println("preparing usernames")
	for _, user := range listUsers(nil) {
		usernames = append(usernames, user["username"].(string))
	}

	log.Println("preparing task types")
	taskTypes = listTaskTypes(nil)

	log.Println("preparing settings")
	settings = SettingsFromConfiguration(configuration)

	log.Println("starting web")
	web := vychuchol.NewWeb(configuration.Port())
	web.Route("/login", login)
	web.Route("/api", api)
	web.Route("/maintenance", maintenance)
	web.Route("/logout", logout)
	web.DefaultRoute(handler)
	web.Start()
}
