package scheduler

import (
	vychuchol "bitbucket.org/vychuchol/core"
)

type Settings struct {
	FreeLogin    bool
	PasswordSalt string
}

func SettingsFromConfiguration(c vychuchol.Configuration) *Settings {
	s := &Settings{}
	if c == nil {
		return s
	}

	so, ok := c["settings"].(map[string]interface{})
	if !ok {
		return s
	}

	s.FreeLogin, _ = so["freelogin"].(bool)
	s.PasswordSalt, _ = so["passwordsalt"].(string)
	return s
}
