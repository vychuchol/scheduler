CREATE INDEX "tasks_year_index" ON "tasks" USING BTREE ("year");

CREATE INDEX "tasks_month_index" ON "tasks" USING BTREE ("month");

CREATE INDEX "tasks_day_index" ON "tasks" USING BTREE ("day");

CREATE INDEX "tasks_hour_index" ON "tasks" USING BTREE ("hour");

CREATE INDEX "tasks_minute_index" ON "tasks" USING BTREE ("minute");

CREATE INDEX "tasks_duration_index" ON "tasks" USING BTREE ("duration");
