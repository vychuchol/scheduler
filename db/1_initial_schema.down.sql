DROP TABLE public.versions;

DROP TABLE public.users;

DROP TABLE public.employees;

DROP TABLE public.roles;

DROP TABLE public.tasks;

DROP TABLE public.task_types;
