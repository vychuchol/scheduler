CREATE TABLE public.versions
(
    version integer,
    created timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (version)
)
WITH (
    OIDS = FALSE
);
