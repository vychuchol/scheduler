CREATE TABLE public.versions
(
    version integer,
    created timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (version)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.versions
    OWNER to scheduler;

CREATE TABLE public.users
(
    id serial,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT now(),
    deleted timestamp without time zone DEFAULT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.users
    OWNER to scheduler;

ALTER TABLE public.users
    ADD UNIQUE (username);

CREATE TABLE public.employees
(
    id serial,
    name character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT now(),
    deleted timestamp without time zone DEFAULT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.employees
    OWNER to scheduler;

ALTER TABLE public.employees
    ADD UNIQUE (name);

CREATE TABLE public.roles
(
    id serial,
    user_id integer NOT NULL,
    employee_id integer NOT NULL,
    name character varying(255) NOT NULL,
    seq integer,
    created timestamp without time zone NOT NULL DEFAULT now(),
    deleted timestamp without time zone DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    FOREIGN KEY (employee_id)
        REFERENCES public.employees (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.roles
    OWNER to scheduler;

ALTER TABLE public.roles
    ADD UNIQUE (user_id, employee_id);

CREATE TABLE public.tasks
(
    id serial,
    year integer NOT NULL,
    month integer NOT NULL,
    day integer NOT NULL,
    hour integer NOT NULL,
    minute integer NOT NULL,
    duration integer NOT NULL,
    user_id integer NOT NULL,
    employee_id integer NOT NULL,
    task_type_id integer NOT NULL DEFAULT 1,
    name character varying(255) NOT NULL,
    description text NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT now(),
    deleted timestamp without time zone DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    FOREIGN KEY (employee_id)
        REFERENCES public.employees (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.tasks
    OWNER to scheduler;

CREATE TABLE public.task_types
(
    id serial,
    name character varying(255) NOT NULL,
    color integer NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT now(),
    deleted timestamp without time zone DEFAULT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.task_types
    OWNER to scheduler;

INSERT INTO task_types (id, name, color) VALUES (1, 'Výchozí', 16777215);

ALTER SEQUENCE task_types_id_seq RESTART WITH 2;
