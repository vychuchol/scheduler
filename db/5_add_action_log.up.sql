CREATE TABLE "public"."action_logs"
(
    "id" serial,
    "action" character varying(16) NOT NULL,
    "object" character varying(16) NOT NULL,
    "user_id" integer NOT NULL,
    "parameters" text NOT NULL,
    "created" timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
		FOREIGN KEY ("user_id")
        REFERENCES "public"."users" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE "public"."action_logs"
    OWNER to "scheduler";
