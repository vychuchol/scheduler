ALTER TABLE "tasks" 
    ADD CONSTRAINT "tasks_task_type_id_fkey"
    FOREIGN KEY (task_type_id)
    REFERENCES public.task_types (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
