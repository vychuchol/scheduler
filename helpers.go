package scheduler

import (
	"regexp"
	"runtime"
	"strconv"
)

var hexColorPattern = regexp.MustCompile("^#[0-9A-Fa-z]{6}$")

func HexColorToInt32(hc string) int32 {
	if len(hc) != 7 {
		return -1
	}
	if !hexColorPattern.MatchString(hc) {
		return -1
	}
	r, err := strconv.ParseInt(hc[1:3], 16, 32)
	if err != nil {
		return -1
	}
	g, err := strconv.ParseInt(hc[3:5], 16, 32)
	if err != nil {
		return -1
	}
	b, err := strconv.ParseInt(hc[5:7], 16, 32)
	if err != nil {
		return -1
	}
	return int32((r << 16) | (g << 8) | b)
}

const unknownCallerName = "<unknown>"

func CallerName(level int) string {
	if level < 0 {
		level = 0
	}

	fs := make([]uintptr, 1)

	n := runtime.Callers(level+2, fs)
	if n == 0 {
		return unknownCallerName
	}

	f := runtime.FuncForPC(fs[0] - 1)
	if f == nil {
		return unknownCallerName
	}

	return f.Name()
}
