"use strict";

function Record(row, task) {
	let record = this;
	record.id = task.id;
	record.employeeID = task.employee_id;
	record.taskTypeID = task.task_type_id;
	record.year = task.year;
	record.month = task.month;
	record.day = task.day;
	record.hour = task.hour;
	record.minute = task.minute;
	record.duration = task.duration;
	record.name = task.name;
	record.description = task.description;
	record.from = task.hour+(task.minute/60);
	record.dom = document.createElement("div");
	record.dom.classList.add("record");
	record.dom.style.fontSize = Sch.configuration.taskFontSizePx();
	record.dom.style.height = Sch.configuration.lineHeightPx();
	record.dom.setAttribute("title", task.description);
	record.dom.setAttribute("data-record-id", task.id);
	Sch.helpers.setTaskType(record.dom, task.task_type_id);
	record.dom.textContent = task.name;
	record.dom.addEventListener("click", Sch.events.clickToRecord);
	record.dom.addEventListener("dblclick", Sch.events.dblclickToRecord);
	Sch.helpers.setPositionAndSize(record.dom, record.from, record.duration);

	record.getDate = function() {
		return Sch.helpers.formatDate(record.year, record.month, record.day);
	};

	record.isOverlappedWith = function(hour, minute, duration) {
		let recordFrom = record.hour*60+record.minute;
		let from = hour*60+minute;

		if (from == recordFrom) {
			return true;
		}
		if (from < recordFrom) {
			return duration > (recordFrom-from);
		}
		// from > recordFrom
		return from < (recordFrom+record.duration);
	};

	row.records.push(record);
	row.dom.appendChild(record.dom);
}

function Modal(configuration) {
	let modal = this;
	modal.id = configuration.id;

	// get content
	let content = document.getElementById(modal.id);
	if (content === null) {
		throw "Invalid id for Modal content: "+modal.id;
	}
	modal.content = {
		dom: content,
	};

	modal.onBeforeSuccess = configuration.onBeforeSuccess || function() { return false; };
	modal.onSuccess = configuration.onSuccess || function() { return true; };
	modal.onSuccessData = configuration.onSuccessData || null;
	modal.onError = configuration.onError || function() {};
	modal.onShow = configuration.onShow || function() {};
	modal.onHide = configuration.onHide || function() {};
	modal.dom = document.createElement("div");
	modal.dom.classList.add("schedulerModal");
	modal.dom.setAttribute("role", "dialog");
	modal.dom.style.display = "none";

	let header = document.createElement("div");
	header.classList.add("header");
	let title = document.createElement("span");
	title.classList.add("title");
	title.textContent = configuration.title || "Modal";
	header.appendChild(title);
	modal.dom.appendChild(header);
	let close = document.createElement("span");
	close.classList.add("close");
	close.setAttribute("title", "Zavřít");
	close.textContent = String.fromCharCode(215);
	close.addEventListener("click", function(evt) {
		evt.cancelBubble = true;
		Modal.overlay.close();
	});
	header.appendChild(close);
	modal.dom.appendChild(content);
	let footer = document.createElement("div");
	footer.classList.add("footer");
	let successButton = document.createElement("button");
	successButton.textContent = configuration.successButtonText || "OK";
	successButton.addEventListener("click", function(evt) {
		evt.cancelBubble = true;
		let beforeSuccessResult = modal.onBeforeSuccess(modal);
		if (beforeSuccessResult !== false) {
			alert(beforeSuccessResult);
			return;
		}
		let result = modal.onSuccess(modal, modal.onSuccessData);
		if (result instanceof Promise) {
			Modal.overlay.waitForPromise(result, function() {
				Sch.reloadTasks();
			}, modal.onError);
			return;
		}
		if (result) {
			modal.close();
		}
	});
	footer.appendChild(successButton);
	modal.dom.appendChild(footer);

	Modal.overlay.initModal(modal);

	modal.open = function() {
		modal.onShow(modal);
		Modal.overlay.open(modal);
	};

	modal.close = function() {
		if (Modal.overlay.isOpen(modal)) {
			Modal.overlay.close();
		}
	};
}

Modal.overlay = {
	dom: null,
	modal: null,
	initModal: function(modal) {
		if (Modal.overlay.dom === null) {
			let overlay = document.createElement("div");
			overlay.style.display = "none";
			overlay.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
			overlay.style.position = "fixed";
			overlay.style.left = 0;
			overlay.style.top = 0;
			overlay.style.right = 0;
			overlay.style.bottom = 0;
			overlay.style.zIndex = 1000;
			overlay.addEventListener("click", function(evt) {
				evt.cancelBubble = true;
				if (evt.target === Modal.overlay.dom) {
					if (Modal.overlay.isOpen()) {
						Modal.overlay.close();
					}
				}
			});
			Modal.overlay.dom = overlay;
			document.body.appendChild(overlay);
		}
		Modal.overlay.dom.appendChild(modal.dom);
	},
	// Open passed modal.
	// Returns nothing.
	open: function(modal) {
		Modal.overlay.modal = modal;
		Modal.overlay.modal.dom.style.display = "block";
		Modal.overlay.show();
	},
	// Check if passed modal is open.
	// If no parameter is specified, check if any modal is open.
	// Returns boolean value.
	isOpen: function(modal) {
		if (modal) {
			return Modal.overlay.modal === modal;
		}
		return Modal.overlay.modal !== null;
	},
	// Close current opened modal but not overlay.
	// If any modal was close, returns true; otherwise returns false.
	// Returns boolean value.
	closeModalOnly: function() {
		if (Modal.overlay.isOpen()) {
			Modal.overlay.modal.dom.style.display = "none";
			Modal.overlay.modal = null;
			return true;
		}
		return false;
	},
	// Close current opened modal and overlay.
	// Returns nothing.
	close: function() {
		let modal = null;
		if (Modal.overlay.isOpen()) {
			modal = Modal.overlay.modal;
		}
		Modal.overlay.closeModalOnly();
		Modal.overlay.hide();
		if (modal !== null) {
			modal.onHide(modal);
		}
	},
	// Show overlay.
	// Returns nothing.
	show: function() {
		Modal.overlay.dom.style.display = "block";
	},
	// Hide overlay.
	// Returns nothing.
	hide: function() {
		Modal.overlay.dom.style.display = "none";
	},
	// Show overlay (and close current opened modal if any).
	// Then waits for passed promise and hide overlay when promise is finalized.
	// After done, runAfter function is run too, if passed any.
	// Returns nothing.
	waitForPromise: function(promise, runAfter, onError) {
		if (!Modal.overlay.closeModalOnly()) {
			Modal.overlay.show();
		}
		promise.then(
			// fulfilled
			function(){
			}, 
			// rejected
			function(error){
				// check if user is logged in
				if (error.data.endsWith("user not logged in")) {
					alert("Přihlášení do systému vypršelo.");
					location.reload();
					return;
				}
				if (onError) {
					onError(error);
				}
			}).finally(function() {
			Modal.overlay.hide();
			if (runAfter) {
				runAfter();
			}
		});
	},
};

function EditTaskModal(id) {
	let etm = this;
	Modal.call(etm, {
		id: id,
		title: "Úkol",
		successButtonText: "Uložit",
		onBeforeSuccess: function(modal) {
			let hour = Number(modal.inputs.hour.value);
			let minute = Number(modal.inputs.minute.value);
			let duration = Math.round(modal.inputs.duration.value*60);
			if (!Sch.isAvailableSpace(modal.employeeID, hour, minute, duration, modal.taskID)) {
				return "Úkol se kryje s jiným nebo je mimo pracovní dobu.";
			}
			return false;
		},
		onSuccess: function(modal) {
			if (modal.taskID != 0) {
				return Sch.API.modifyTask(
					modal.taskID,
					modal.employeeID,
					modal.inputs.taskType.value,
					modal.inputs.name.value,
					modal.inputs.description.value,
					modal.date,
					modal.inputs.hour.value,
					modal.inputs.minute.value,
					Math.round(modal.inputs.duration.value*60),
				);
			}

			return Sch.API.createTask(
				modal.employeeID,
				modal.inputs.taskType.value,
				modal.inputs.name.value,
				modal.inputs.description.value,
				modal.date,
				modal.inputs.hour.value,
				modal.inputs.minute.value,
				Math.round(modal.inputs.duration.value*60),
			);
		},
	});

	etm.taskID = 0;
	etm.employeeID = 0;
	etm.date = Sch.today.format("YYYY-MM-DD");

	etm.inputs = {
		name:        document.querySelector("#"+id+" input[name='name']"),
		description: document.querySelector("#"+id+" textarea[name='description']"),
		taskType:    document.querySelector("#"+id+" select[name='taskType']"),
		date:        document.querySelector("#"+id+" input[name='date']"),
		hour:        document.querySelector("#"+id+" input[name='hour']"),
		minute:      document.querySelector("#"+id+" input[name='minute']"),
		duration:    document.querySelector("#"+id+" input[name='duration']"),
	};

	// initialisation of inputs
	etm.inputs.hour.min = Sch.configuration.hours.from;
	etm.inputs.hour.max = Sch.configuration.hours.to;
	etm.inputs.minute.step = Sch.configuration.recordTimePrecision;
	etm.inputs.duration.step = Sch.configuration.recordDurationPrecision;
	etm.inputs.duration.max = Sch.configuration.hours.count()*60;

	etm.setTask = function(record) {
		etm.taskID = record.id;
		etm.employeeID = record.employeeID;
		etm.inputs.name.value = record.name;
		etm.inputs.description.value = record.description;
		etm.inputs.taskType.value = record.taskTypeID;
		etm.setDate(record.getDate());
		etm.setTime(record.hour, record.minute, record.duration);
	};
	etm.clearTask = function() {
		etm.taskID = 0;
		etm.employeeID = 0;
		etm.inputs.name.value = "";
		etm.inputs.description.value = "";
		etm.inputs.taskType.value = 1; // default task type
		etm.date = Sch.today.format("YYYY-MM-DD");
		etm.setTime(0, 0, 0);
	};
	etm.setDate = function(date) {
		etm.date = date;
		etm.inputs.date.value = moment(date, "YYYY-MM-DD").format(Sch.configuration.dateFormat);
	};
	etm.setTime = function(hour, minute, duration) {
		etm.inputs.hour.value = hour;
		etm.inputs.minute.value = minute;
		etm.inputs.duration.value = duration/60;
	};
}

function MoveTaskModal(id) {
	let mtm = this;
	Modal.call(mtm, {
		id: id,
		title: "Přesunout úkol",
		successButtonText: "Přesunout",
		onBeforeSuccess: function(modal) {
			// TODO New endpoint can_move_task.
			if (false) {
				return "Úkol se kryje s jiným nebo je mimo pracovní dobu.";
			}
			return false;
		},
		onSuccess: function(modal) {
			return Sch.API.moveTask(
				modal.taskID,
				modal.date,
			);
		},
		onError: function(error) {
			alert("Úkol nelze přesunout.");
		},
		onShow: function(modal) {
			modal.picker.show();
		},
		onHide: function(modal) {
			modal.picker.hide();
		},
	});

	mtm.taskID = 0;
	mtm.date = Sch.today.format("YYYY-MM-DD");

	mtm.inputs = {
		name: document.querySelector("#"+id+" input[name='name']"),
		date: document.querySelector("#"+id+" input[name='date']"),
	};

	mtm.picker = Sch.helpers.newPikaday(mtm.inputs.date, mtm.content.dom, function(date) {
		mtm.date = moment(date).format("YYYY-MM-DD");
	});
	mtm.picker.hide();

	mtm.setTask = function(record) {
		mtm.taskID = record.id;
		mtm.inputs.name.value = record.name;
		mtm.setDate(record.getDate());
	};
	mtm.setDate = function(date) {
		mtm.date = date;
		mtm.inputs.date.value = moment(date, "YYYY-MM-DD").format(Sch.configuration.dateFormat);
		mtm.picker.setDate(date);
	};
}

function ReassignTaskModal(id) {
	let rtm = this;
	Modal.call(rtm, {
		id: id,
		title: "Předat úkol",
		successButtonText: "Předat",
		onBeforeSuccess: function(modal) {
			if (modal.inputs.employee.value == 0) {
				return "Vyberte zaměstnance pro předání úkolu.";
			}
			return false;
		},
		onSuccess: function(modal) {
			return Sch.API.reassignTask(
				modal.taskID,
				modal.inputs.employee.value,
			);
		},
		onError: function(error) {
			alert("Úkol nelze předat.");
		},
	});

	rtm.taskID = 0;

	rtm.inputs = {
		name: document.querySelector("#"+id+" input[name='name']"),
		employee: document.querySelector("#"+id+" select[name='employee']"),
	};

	rtm.setTask = function(record) {
		rtm.taskID = record.id;
		rtm.inputs.name.value = record.name;
		rtm.inputs.employee.value = 0;
	};
	rtm.setListOfEmployees = function(list) {
		while (rtm.inputs.employee.firstChild) {
			rtm.inputs.employee.removeChild(rtm.inputs.employee.firstChild);
		}
		for (let employee of list) {
			let option = document.createElement("option");
			option.setAttribute("value", employee.id);
			option.textContent = employee.name;
			rtm.inputs.employee.appendChild(option);
		}
	};
}

function ViewTaskModal(id) {
	let vtm = this;
	Modal.call(vtm, {
		id: id,
		title: "Detail úkolu",
		successButtonText: "Zavřít",
	});

	vtm.taskID = 0;

	vtm.inputs = {
		name:        document.querySelector("#"+id+" input[name='name']"),
		description: document.querySelector("#"+id+" textarea[name='description']"),
	};

	vtm.setTask = function(record) {
		vtm.taskID = record.id;
		vtm.inputs.name.value = record.name;
		vtm.inputs.description.value = record.description;
	};
}

function ActionButton(element, action) {
	let actionButton = this;
	let dom = element;
	if (typeof element === "string") {
		dom = document.getElementById(element);
	}
	if (!(dom instanceof HTMLButtonElement)) {
		throw "Expected HTMLButtonElement as parameter of ActionButton, passed: "+element;
	}

	actionButton.dom = dom;
	actionButton.isDisabled = function() {
		return actionButton.dom.hasAttribute("disabled");
	};
	actionButton.setDisabled = function(state) {
		if (state) {
			actionButton.dom.setAttribute("disabled", "disabled");
		} else {
			actionButton.dom.removeAttribute("disabled");
		}
	};
	actionButton.action = action;
	actionButton.click = function() {
		if (typeof action !== "function") {
			throw "Property action in ActionButton must be function.";
		}
		actionButton.action(actionButton);
	};
	actionButton.dom.addEventListener("click", actionButton.click);
}

let Sch = {
	loaded: false,
	API: {
		lastCallID: 1,
	},
	actions: {
		functions: {},
		buttons: {},
	},
	events: {},
	helpers: {},
	taskTypes: [],
	today: null,
	selectedRecord: null,
};

// API

Sch.API.call = function(method, parameters) {
	return new Promise(function(resolve, reject) {
		let id = Sch.API.lastCallID++;
		let xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (this.status == 200) {
					try {
						let json = JSON.parse(this.responseText);
						if (json.error) {
							reject(json.error);
							return;
						}
						resolve(json);
					} catch(e) {
						// TODO
						reject({});
					}
				} else {
					// TODO
					reject({});
				}
			}
		};
		xhttp.open("POST", "/api", true);
		xhttp.send(JSON.stringify({
				"jsonrpc": "2.0", 
				"method": method, 
				"params": parameters, 
				"id": id.toString(),
		}));
	});
};

Sch.API.listEmployees = function(userID) {
	return Sch.API.call("list_employees", {user_id: userID});
};

Sch.API.getTasks = function(date) {
	return Sch.API.call("get_tasks", {date: date});
};

Sch.API.createTask = function(employeeID, taskTypeID, name, description, date, hour, minute, duration) {
	return Sch.API.call("create_task", {
		date_time:    date+"T"+Sch.helpers.formatTime(hour, minute)+"Z",
		duration:     "PT"+duration+"M",
		employee_id:  employeeID,
		task_type_id: taskTypeID,
		name:         name,
		description:  description,
	});
};

Sch.API.modifyTask = function(taskID, employeeID, taskTypeID, name, description, date, hour, minute, duration) {
	return Sch.API.call("modify_task", {
		task_id:      taskID,
		date_time:    date+"T"+Sch.helpers.formatTime(hour, minute)+"Z",
		duration:     "PT"+duration+"M",
		employee_id:  employeeID,
		task_type_id: taskTypeID,
		name:         name,
		description:  description,
	});
};

Sch.API.moveTask = function(taskID, date) {
	return Sch.API.call("move_task", {
		task_id: taskID,
		date:    date,
	});
};

Sch.API.reassignTask = function(taskID, employeeID) {
	return Sch.API.call("reassign_task", {
		task_id:     taskID,
		employee_id: employeeID,
	});
};

Sch.API.deleteTask = function(taskID) {
	return Sch.API.call("delete_task", {
		task_id: taskID,
	});
};

// Actions

Sch.actions.functions["Delete"] = function() {
	if (!Sch.hasSelectedRecord()) {
		return;
	}

	if (confirm("Odstranit vybraný úkol?")) {
		let result = Sch.API.deleteTask(Sch.selectedRecord.id);
		Modal.overlay.waitForPromise(result, function() {
			Sch.reloadTasks();
		});
	}
};

Sch.actions.functions["Edit"] = function() {
	if (!Sch.hasSelectedRecord()) {
		return;
	}

	// open edit task modal with specific record
	let record = Sch.data.records[Sch.selectedRecord.id];
	Sch.modals.editTask.setTask(record);
	Sch.modals.editTask.open();
};

Sch.actions.functions["Move"] = function() {
	if (!Sch.hasSelectedRecord()) {
		return;
	}

	// open move task modal with specific record
	let record = Sch.data.records[Sch.selectedRecord.id];
	Sch.modals.moveTask.setTask(record);
	Sch.modals.moveTask.open();
};

Sch.actions.functions["Reassign"] = function() {
	if (!Sch.hasSelectedRecord()) {
		return;
	}

	// open move task modal with specific record
	let record = Sch.data.records[Sch.selectedRecord.id];
	Sch.modals.reassignTask.setTask(record);
	Sch.modals.reassignTask.open();
};

Sch.actions.functions["NextDay"] = function() {
	let date = Sch.today.add(1, "days").format("YYYY-MM-DD");
	Sch.setDate(date);
};

Sch.actions.functions["PreviousDay"] = function() {
	let date = Sch.today.subtract(1, "days").format("YYYY-MM-DD");
	Sch.setDate(date);
};

Sch.actions.functions["View"] = function() {
	if (!Sch.hasSelectedRecord()) {
		return;
	}

	// open view task modal with specific record
	let record = Sch.data.records[Sch.selectedRecord.id];
	Sch.modals.viewTask.setTask(record);
	Sch.modals.viewTask.open();
};

// Events

Sch.events.clickToRow = function(evt) {
	evt.cancelBubble = true;

	let employeeID = evt.target.getAttribute("data-employee-id");
	if (!Sch.isEditorOfEmployee(employeeID)) {
		return;
	}

	// calculate x position of click (relative to element)
	let rect = evt.target.getBoundingClientRect();
	let x = evt.clientX-rect.left;
	let width = rect.width;
	let position = x/width;

	let firstHour = Sch.configuration.hours.from;
	let hours = Sch.configuration.hours.count();

	// calculate time related to click position
	let time = (position*hours)+firstHour;
	let hour = Math.floor(time);
	let minute = Math.floor((time-hour)*60);

	// round to nearest time span (depends on configuration)
	let p = Sch.configuration.newRecordPrecision;
	minute = Math.floor(minute/p)*p;

	// open task modal with calculated time
	Sch.modals.editTask.clearTask(hour, minute);
	Sch.modals.editTask.employeeID = employeeID;
	Sch.modals.editTask.setDate(Sch.today.format("YYYY-MM-DD"));
	Sch.modals.editTask.setTime(hour, minute, Sch.configuration.defaultTaskDuration);
	Sch.modals.editTask.open();
};

Sch.events.clickToRecord = function(evt) {
	evt.cancelBubble = true;

	Sch.setSelectedRecord(evt.target.getAttribute("data-record-id"));
};

Sch.events.dblclickToRecord = function(evt) {
	evt.cancelBubble = true;

	// open edit task modal with specific record
	// see: edit action
	let recordID = evt.target.getAttribute("data-record-id");
	let record = Sch.data.records[recordID];
	Sch.modals.editTask.setTask(record);
	Sch.modals.editTask.open();
};

// Helpers

Sch.helpers.appendDiv = function(parent, text) {
	let div = document.createElement("div");
	if (text) {
		div.textContent = text;
	}
	parent.appendChild(div);
	return div;
};

Sch.helpers.formatDate = function(year, month, day) {
	month = (month > 9) ? month.toString() : "0"+month;
	day = (day > 9) ? day.toString() : "0"+day;
	return year+"-"+month+"-"+day;
};

Sch.helpers.formatTime = function(hour, minute, second) {
	hour = (hour > 9) ? hour.toString() : "0"+hour;
	minute = (minute > 9) ? minute.toString() : "0"+minute;
	if (second) {
		second = (second > 9) ? second.toString() : "0"+second;
	} else {
		second = "00";
	}
	return hour+":"+minute+":"+second;
};

Sch.helpers.newPikaday = function(element, container, onSelect) {
	var parameters = {
		disableWeekends: true,
		disableDayFn: function(date) {
			let year = date.getFullYear();
			let month = date.getMonth() + 1;
			let day = date.getDate();
			return IsHoliday(year, month, day) !== null;
		},
		field: element,
		firstDay: 1,
		format: Sch.configuration.dateFormat,
		i18n: {
			previousMonth   : 'Předchozí měsíc',
			nextMonth       : 'Následující měsíc',
			months          : ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
			weekdays        : ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
			weekdaysShort   : ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
		},
		onSelect: onSelect || function() {},
	};
	if (container) {
		parameters.bound = false;
		parameters.container = container;
	}
	return new Pikaday(parameters);
};

Sch.helpers.setPositionAndSize = function(dom, from, duration) {
	// calculate position and size
	let firstHour = Sch.configuration.hours.from;
	let hours = Sch.configuration.hours.count();
	dom.style.left = (((from-firstHour)/hours)*100)+"%";
	dom.style.width = (((duration/60)/hours)*100)+"%";
};

Sch.helpers.setTaskType = function(dom, taskTypeID) {
	let taskType = Sch.getTaskType(taskTypeID);
	if (taskType === null) {
		console.log("invalid task type id: "+taskTypeID);
		return;
	}
	dom.style.backgroundColor = taskType.color;
};

// Methods

Sch.initData = function() {
	Sch.data = {
		// all rows (key is employee id)
		rows: {},
		// all records (key is task id)
		records: {},
	};
};

Sch.init = function() {
	// configuration
	Sch.configuration.hours.count = function() {
		return Math.max(0, this.to-this.from+1);
	};
	
	Sch.configuration.hours.duration = function() {
		return Math.max(0, this.to-this.from)*60;
	};
	
	Sch.configuration.lunchBreak.count = function() {
		return Math.max(0, this.to-this.from+1);
	};
	
	Sch.configuration.lunchBreak.duration = function() {
		return Math.max(0, this.to-this.from)*60;
	};

	Sch.configuration.lineHeightPx = function() {
		return this.lineHeight+"px";
	};

	Sch.configuration.taskFontSizePx = function() {
		return this.taskFontSize+"px";
	};

	// fetch employees
	Sch.API.listEmployees(Sch.userID).then(function(resp) {
		Sch.today = moment();
		Sch.initData();

		Sch.employees = resp.result.employees;

		// generate table
		let table = document.createElement("div");
		table.classList.add("scheduler");
		Sch.table = {dom: table};
		document.getElementsByTagName("main")[0].appendChild(table);

		let left = document.createElement("div");
		left.classList.add("left");
		Sch.table.left = {dom: left};
		Sch.table.dom.appendChild(left);

		let corner = Sch.helpers.appendDiv(Sch.table.left.dom);
		let datepicker = document.createElement("input");
		datepicker.classList.add("datepicker");
		datepicker.setAttribute("readonly", "readonly");
		datepicker.setAttribute("type", "text");
		datepicker.setAttribute("value", Sch.today.format(Sch.configuration.dateFormat));
		corner.appendChild(datepicker);
		Sch.table.corner = {
			dom: corner,
			datepicker: {
				dom: datepicker,
				getDate: function() {
					return moment(Sch.table.corner.datepicker.picker.getDate()).format("YYYY-MM-DD");
				},
				picker: Sch.helpers.newPikaday(datepicker, null, function(date) {
					if (Sch.table.corner.datepicker.picker.blocked) {
						return;
					}
					Sch.setDate(moment(date).format("YYYY-MM-DD"));
				}),
				blocked: false,
			},
		};

		let content = document.createElement("div");
		content.classList.add("content");
		Sch.table.content = {dom: content};
		Sch.table.dom.appendChild(content);

		let header = document.createElement("div");
		header.classList.add("header");
		let fromHour = Sch.configuration.hours.from;
		let toHours = Sch.configuration.hours.to;
		for (let hour = fromHour; hour <= toHours; hour++) {
			Sch.helpers.appendDiv(header, hour+":00");
		}
		Sch.table.header = {dom: header};
		Sch.table.content.rows = [];
		Sch.table.content.dom.appendChild(header);

		for (let i = 0; i < Sch.employees.length; i++) {
			let employee = Sch.employees[i];

			let lineHeightPx = Sch.configuration.lineHeightPx();

			let div = Sch.helpers.appendDiv(Sch.table.left.dom, employee.name);
			div.style.height = lineHeightPx;
			div.style.lineHeight = lineHeightPx;
			div.style.minHeight = lineHeightPx;

			let row = {employee: employee, records: []};
			row.dom = document.createElement("div");
			row.dom.classList.add("row");
			row.dom.style.height = lineHeightPx;
			row.dom.style.lineHeight = lineHeightPx;
			row.dom.style.minHeight = lineHeightPx;
			row.dom.setAttribute("data-employee-id", employee.id);
			row.dom.addEventListener("click", Sch.events.clickToRow);
			Sch.data.rows[employee.id] = row;
			Sch.table.content.rows.push(row);
			Sch.table.content.dom.appendChild(row.dom);
		}

		Sch.modals = {
			editTask:     new EditTaskModal("schedulerEditTask"),
			moveTask:     new MoveTaskModal("schedulerMoveTask"),
			reassignTask: new ReassignTaskModal("schedulerReassignTask"),
			viewTask:     new ViewTaskModal("schedulerViewTask"),
		};

		// fill list of employees to reassign task modal
		let listOfEmployees = [];
		for (let employee of Sch.employees) {
			if (employee.role != "editor") {
				continue;
			}
			listOfEmployees.push(employee);
		}
		Sch.modals.reassignTask.setListOfEmployees(listOfEmployees);

		Sch.bindAction("actionButtonPreviousDay", "PreviousDay");
		Sch.bindAction("actionButtonView",        "View");
		Sch.bindAction("actionButtonEdit",        "Edit");
		Sch.bindAction("actionButtonMove",        "Move");
		Sch.bindAction("actionButtonReassign",    "Reassign");
		Sch.bindAction("actionButtonDelete",      "Delete");
		Sch.bindAction("actionButtonNextDay",     "NextDay");

		// refresh actions buttons for selected records
		Sch.setSelectedRecord(0);

		// show whole page content
		document.getElementsByTagName("main")[0].style.display = "block";
		Sch.loaded = true;

		Sch.setDate(Sch.today.format("YYYY-MM-DD"));
	});
}

Sch.bindAction = function(element, action) {
	if (!Sch.hasAction(action)) {
		throw "Parameter action in bindAction must be valid action, passed: "+action;
	}
	if (!Sch.actions.buttons.hasOwnProperty(action)) {
		Sch.actions.buttons[action] = [];
	}
	Sch.actions.buttons[action].push(new ActionButton(element, function() {
		Sch.doAction(action);
	}));
};

Sch.hasAction = function(action) {
	return Sch.actions.functions.hasOwnProperty(action);
};

Sch.doAction = function(action) {
	if (!Sch.hasAction(action)) {
		throw "Parameter action in doAction must be valid action, passed: "+action;
	}
	Sch.actions.functions[action]();
};

// Returns true if there is space for record at current day.
// Othervise returns false.
Sch.isAvailableSpace = function(employeeID, hour, minute, duration, ignoreTaskID) {
	// check configured hours
	if (hour < Sch.configuration.hours.from) {
		return false;
	}
	if (hour > Sch.configuration.hours.to) {
		return false;
	}
	if ((hour*60+minute+duration) > ((Sch.configuration.hours.to+1)*60)) {
		return false;
	}

	let row = Sch.data.rows[employeeID];
	for (let record of row.records) {
		if (record.id == ignoreTaskID) {
			continue;
		}
		if (record.isOverlappedWith(hour, minute, duration)) {
			return false;
		}
	}

	return true;
};

Sch.isEditorOfEmployee = function(employeeID) {
	let missingEmployee = true;
	for (let employee of Sch.employees) {
		if (employee.id == employeeID) {
			if (employee.role !== 'editor') {
				return false;
			}
			missingEmployee = false;
			continue;
		}
	}
	if (missingEmployee) {
		console.log("missing employee with id: "+employeeID);
		return false;
	}
	return true;
};

Sch.hasSelectedRecord = function() {
	return Sch.selectedRecord !== null;
};

Sch.setSelectedRecord = function(id) {
	let refreshActionButtons = function(blockActions) {
		let selected = Sch.hasSelectedRecord();
		for (let button of Sch.actions.buttons["View"]) {
			button.setDisabled(!selected);
		}

		let disabled = !selected || blockActions;
		let disableableActions = [
			"Edit",
			"Move",
			"Reassign",
			"Delete",
		];
		for (let action of disableableActions) {
			for (let button of Sch.actions.buttons[action]) {
				button.setDisabled(disabled);
			}
		}
	};

	if (Sch.selectedRecord !== null) {
		if (Sch.selectedRecord.id == id) {
			return;
		}
		Sch.selectedRecord.dom.classList.remove("selected");
	}

	let record = Sch.data.records[id];
	if (!record) {
		Sch.selectedRecord = null;
		refreshActionButtons();
		return;
	}
	let dom = Sch.table.dom.querySelector("*[data-record-id=\""+id+"\"]");
	if (dom) {
		dom.classList.add("selected");
		if (Sch.selectedRecord === null) {
			Sch.selectedRecord = {};
		}
		Sch.selectedRecord.id = id;
		Sch.selectedRecord.dom = dom;
	}
	refreshActionButtons(!Sch.isEditorOfEmployee(record.employeeID));
};

Sch.getTaskType = function(id) {
	for (let taskType of Sch.taskTypes) {
		if (taskType.id === id) {
			return taskType;
		}
	}
	return null;
};

Sch.reloadTasks = function() {
	Sch.setDate(Sch.today.format("YYYY-MM-DD"));
};

Sch.setDate = function(date) {
	let result = Sch.API.getTasks(date).then(function(resp) {
		Sch.today = moment(date, "YYYY-MM-DD");
		let year = Number(Sch.today.format("YYYY"));
		let month = Number(Sch.today.format("M"));
		let day = Number(Sch.today.format("D"));
		Sch.table.corner.datepicker.picker.blocked = true;
		Sch.table.corner.datepicker.picker.setMoment(Sch.today);
		Sch.table.corner.datepicker.picker.blocked = false;
		// clear  all tasks
		for (let row of Object.values(Sch.data.rows)) {
			while (row.dom.hasChildNodes()) {
				row.dom.removeChild(row.dom.lastChild);
			}
			row.records = [];
		}
		Sch.data.records = {};
		Sch.setSelectedRecord(0);
		// create lunch break
		for (let row of Object.values(Sch.data.rows)) {
			let lunchBreak = document.createElement("div");
			lunchBreak.style.height = Sch.configuration.lineHeightPx();
			lunchBreak.setAttribute("class", "lunchBreak");
			Sch.helpers.setPositionAndSize(lunchBreak, Sch.configuration.lunchBreak.from,  Sch.configuration.lunchBreak.duration());
			row.dom.appendChild(lunchBreak);
		}
		// create received tasks
		let tasks = resp.result.tasks;
		for (let i = 0; i < tasks.length; i++) {
			let task = tasks[i];
			let row = Sch.data.rows[task.employee_id];
			task.year = year;
			task.month = month;
			task.day = day;
			Sch.data.records[task.id] = new Record(row, task);
		}
	});
	Modal.overlay.waitForPromise(result);
};
