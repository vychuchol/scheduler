function IsHoliday(year, month, day) {
	let holidays = {
		1: {
			1: "Nový rok, Den obnovy samostatného českého státu",
		},
		2: {
		},
		3: {
		},
		4: {
		},
		5: {
			1: "Svátek práce",
			8: "Den vítězství",
		},
		6: {
		},
		7: {
			5: "Den slovanských věrozvěstů Cyrila a Metoděje",
			6: "Den upálení mistra Jana Husa",
		},
		8: {
		},
		9: {
			28: "Den české státnosti",
		},
		10: {
			28: " Den vzniku samostatného československého státu",
		},
		11: {
			17: "Den boje za svobodu a demokracii",
		},
		12: {
			24: "Štědrý den",
			25: "1. svátek vánoční",
			26: "2. svátek vánoční",
		},
	};
	if (!month in holidays) {
		console.error("invalid date: "+year+"-"+month+"-"+day);
		return null;
	}
	if (day in holidays[month]) {
		return holidays[month][day];
	}

	// check Easter
	let y = Number(year);
	let a = y % 19;
	let b = y % 4;
	let c = y % 7;
	let m = 24; // for 21. century
	let n = 5; // for 21. century
	let d = (19*a + m) % 30;
	let e = (n + 2*b + 4*c + 6*d) % 7;
	let u = d + e - 9;
	let v = 0;
	if (u == 25 && d == 28 && e == 6 && a > 10) {
		u = 18;
		v = 4;
	} else if (u >= 1 && u <= 25) {
		v = 4;
	} else if (u > 25) {
		u -= 7;
		v = 4;
	} else {
		u = 22 + d + e;
		v = 3;
	}
	// Easter Sunday: year-v-u
	
	// Easter Monday
	let easterMondayD = u + 1;
	let easterMondayM = v;
	if (easterMondayD > 31) {
		easterMondayD = 1;
		easterMondayM++;
	}
	if (easterMondayD == day && easterMondayM == month) {
		return "Velikonoční pondělí";
	}

	// Easter Friday
	let easterFridayD = u - 2;
	let easterFridayM = v;
	if (easterFridayD < 1) {
		easterFridayD = 31 + easterFridayD;
		easterFridayM--;
	}
	if (easterFridayD == day && easterFridayM == month) {
		return "Velký pátek";
	}

	return null;
}
