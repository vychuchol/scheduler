Sch.configuration = {
	dateFormat: "ddd Do Mo YYYY",
	newRecordPrecision: 15, // in minutes
	defaultTaskDuration: 30, // in minutes
	recordTimePrecision: 15, // in minutes
	recordDurationPrecision: 0.25, // in hours
	hours: {
		from: 7,
		to: 17,
	},
	lunchBreak: {
		from: 11,
		to: 11.5,
	},
	lineHeight: 100, // in pixels
	taskFontSize: 16, // in pixels
};
